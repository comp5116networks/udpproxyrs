package Server;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import Utilities.*;

public class UDPProxyServer {

    private static final ArrayList<String> clients = new ArrayList<String>();

    public static void main(String[] args)
    {
//        boolean corruptFlag = false;
//        boolean ISSConnection = true;
//        String corruptionString = "OMG THE DATA IS BEING CORRUPTED BY A SOLAR FLARE";
//        byte [] corruptionData = corruptionString.getBytes();

        if (args.length != 1)
        {
            System.err.println("Invalid number of arguments, Port number only required");
            System.exit(0);
        }
        int port = Integer.parseInt(args[0]);
        DatagramSocket server = null;
        try {
            server = new DatagramSocket(port);
        } catch (IOException e) {
            System.out.println("FATAL ERROR: Unable to open server socket on port " + port);
            e.printStackTrace();
            System.exit(0);
        }
        byte[] request = new byte[1024 + 100];
        byte[] reply = new byte[1024];

        while (true)
        {
            // Receive data from client
            DatagramPacket client = new DatagramPacket(request, request.length);
            try
            {
                server.receive(client);
            } catch (IOException e)
            {
                System.out.println("Error occurred while receiving datagram packet from client");
                e.printStackTrace();
                continue;
            }

//            /*
//             * Date manipulation section
//             */
//            long time = System.currentTimeMillis();
//
//            // every 10 seconds toggle the ISS connection
//            if(time % 20000 < 10000)
//            {
//                continue; // this drops the packets
//            }
//
//            // set data corrupt flag to true for half a second every 2 seconds
//
//            if (!corruptFlag && time%2000 < 500)
//            {
//                corruptFlag = true;
//            }
//            else if (corruptFlag && time%2000 > 500)
//            {
//                corruptFlag=false;
//            }
//
            byte[] payload = client.getData();
//
//            if(corruptFlag && payload.length > 200) // 200 to avoid IndexOutOfBounds
//            {
//                for (int i = 40; i-40 < corruptionData.length; i++) //Minus 40 is so I dont corrupt near the header
//                {
//                    payload[i] = corruptionData[i-40];
//                }
//            }

            /*
            By analysing the packet header determine where the packet needs to be sent
            potentially reply to the client telling them the packet was received by the proxy server
            */
            InetAddress destIP = null; // IP address of destination here
            int destPort = 0;

            Decapsulator decap = new Decapsulator(payload);

            //addToClients(client.getAddress().getHostAddress());
            addToClients(decap.getSourceAddr().getHostAddress(),decap.getSourcePort());
            destIP = decap.getDestAddr();
            destPort = decap.getDestPort();

            if (destIP.getHostAddress().equals("127.0.1.1")) //Client accidentally put source addr as loopback
            {
                destIP = client.getAddress();
            }
            if (!clients.contains(destIP.getHostAddress() + Integer.toString(destPort)))
            {
                System.err.println("Destination IP address not found on list of available clients - "
                                           + destIP.getHostAddress() + destPort);
                continue;
            }

            // Forward the packet to its correct destination
            DatagramPacket send = new DatagramPacket(request, request.length, destIP, destPort);
            try
            {
                server.send(send);
            } catch (IOException e)
            {
                System.out.println("Error occurred while forwarding packet to server");
                e.printStackTrace();
                continue;
            }
        }
    }

    private static void addToClients(String address, int port)
    {

        if (clients.contains(address + Integer.toString(port)))
        {
            return;
        }
        else
        {
            System.out.println("Adding client " + address + " to list of clients");
            clients.add(address + Integer.toString(port));
            clients.add(address + Integer.toString(port+1));
        }
        for (String addr: clients)
        {
            System.out.println(addr);
        }
    }
}
