package Client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import Utilities.Decapsulator;
import Utilities.Encapsulator;
import Utilities.SimulatedNetworkConnection;

public class ReceiverSender {

	private int sendingPort;
	private int PAYSIZE = 1024;
	private int proxyPort;
	private InetAddress proxyIP;
	DatagramSocket socket;
	int delay;
	int headerSize = 49;
	int totalPackSize = 1024 + headerSize;
	static int z = 0;
	int windowsize = 8;
	SimulatedNetworkConnection sim;
	// TODO make this take an arg value
	static int offset = 0;

	/**
	 * 
	 * @param sPort
	 *            port for the sender (the receiver receives on this port +1)
	 * @param pIP
	 *            IP of the proxy
	 * @param pPort
	 *            port of the proxy
	 * @param maxThroughput
	 *            in kilobits per second
	 */
	public ReceiverSender(int sPort, InetAddress pIP, int pPort,
			int maxThroughput) {
		float throughputInKiloBytesPerSec = (float) maxThroughput / (float) 8;
		float d = 1000 / throughputInKiloBytesPerSec;
		d = d + 2;
		delay = (int) d;
		sendingPort = sPort;
		proxyIP = pIP;
		proxyPort = pPort;
		try {
			socket = new DatagramSocket(sendingPort);
			sim = new SimulatedNetworkConnection(offset, socket);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		// program arguments:
		// args[0]= sending port for sender receiver
		// args[1]= IP of proxy server
		// args[2]= Port of proxy server
		// args[3]= maximum throughput in kilobits/s
		if (args.length != 5) {
			System.err.println("Incorrect number of commandline arguments.");
			System.err
			 .println("Run this program with the command line arguments:sending port for sender receiver, IP of proxy server, port of proxy server, Connection Speed, Client offset for simulation 0-60");
			return;
		}
		InetAddress p;
		try {
			offset = Integer.parseInt(args[4]);
			p = InetAddress.getByName(args[1]);
			ReceiverSender rs = new ReceiverSender(Integer.parseInt(args[0]),
					p, Integer.parseInt(args[2]), Integer.parseInt(args[3]));
			rs.run();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void run() {
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(
				System.in));
		String command;
		// spawn a new thread to listen for incoming connections
		DatagramSocket receiverSocket;
		Thread rt;
		try {
			receiverSocket = new DatagramSocket(sendingPort + 1);
			ReceiverThread r = new ReceiverThread(receiverSocket, proxyPort,
					proxyIP);
			rt = new Thread(r);
			rt.start();
			startUpSend();
			System.out.println("Receiver/Sender up and running at: "
					+ InetAddress.getLocalHost().getHostAddress()
					+ " sending on port: " + sendingPort
					+ " receiving on port: " + (sendingPort + 1));
			while (true) {
				try {
					command = stdIn.readLine();
					//System.out.println("read input: " + command);
					if (command.toUpperCase().startsWith("SEND")) {
						send(command);
					} else if (command.toUpperCase().startsWith("EXIT")) {
						System.out.println("Exiting");
						// close the thread and its resources
						exit(rt, receiverSocket);
						// break out of while loop
						break;
					} else if (command.toUpperCase().startsWith("PING")) {
						ping(command);
					} else {
						System.err.println(command + " not recognised");
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			System.err.println("Error creating new socket for receiving");
			e1.printStackTrace();
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * Sends a packet containing no data just to send to itself at start up
	 * through the proxy so that the proxy can add the client to its list of
	 * clients.
	 */
	private void startUpSend() {
		// TODO Auto-generated method stub
		byte[] header = new byte[headerSize];
		byte[] payload = new byte[1024];
		byte[] pack = new byte[totalPackSize];
		DatagramPacket packet;
		try {
			// socket = new DatagramSocket(sendingPort);
			InetAddress sourceIP = InetAddress.getLocalHost();
			byte[] socketAddr = sourceIP.getAddress();
			String destIP = sourceIP.getHostAddress();
			System.out.println(destIP);
			Encapsulator e = new Encapsulator(destIP, "",
					Integer.toString(sendingPort + 1), sendingPort, socketAddr,
					0, 0L, 0L, "STRT");
			header = e.getHeader();
			if (header == null) {
				System.err
						.println("An error occurred creating the header for the packet. ");
				if (socket != null) {
					socket.close();
				}
				return;
			}
			pack = e.concat(header, payload);
			//System.out.println("pack.length " + pack.length);
			packet = new DatagramPacket(pack, pack.length,
					new InetSocketAddress(proxyIP, proxyPort));
			sim.sendAfterSim(packet);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * 
	 * @param command
	 *            format: ping destinationAddress destinationPort numberOfPings
	 */
	public void ping(String command) {
		String[] s = command.split("\\s");
		if (s.length != 4) {
			System.err
					.println("Incorrect number of arguments supplied to ping command. ("
							+ s.length
							+ ") supplied.  format: ping destinationAddress destinationPort");
			return;
		}
		byte[] header = new byte[headerSize];
		byte[] pack;

		DatagramPacket packet;

		String destAddr = s[1];
		String destPort = s[2];
		int pings = Integer.parseInt(s[3]);

		byte[] socketAddr;
		try {
			socketAddr = InetAddress.getLocalHost().getAddress();
			// ByteBuffer.allocate(8).putLong(System.currentTimeMillis()).array();
			if (pings > 1) {

				int replies = 0;
				for (int i = 1; i <= pings; i++) {
					//byte[] payload = putLong(System.currentTimeMillis());
					byte[] payload = putLong(System.currentTimeMillis());
					long before = System.currentTimeMillis();
					// format of pingn command:
					// PINGN number_of_pings this_ping number
					// id_of_this_pingn_command
					// the id is used in the receiver thread to put the number
					// of pings
					// received in a map according to this id so that the total
					// numbver
					// of pings received can be calculated for packet loss
					// calculations

					String name = "PINGN ";

					Checksum checksum = new CRC32();
					checksum.update(payload, 0, payload.length);
					long lngChecksum = checksum.getValue();
					//System.out.println("sending checksum: " + lngChecksum);
					Encapsulator e = new Encapsulator(destAddr, name, destPort,
							sendingPort, socketAddr, 0, lngChecksum, 0L,
							"NPNG");
					header = e.getHeader();
					if (header == null) {
						System.err
								.println("An error occurred creating the header for the packet. ");
						if (socket != null) {
							socket.close();
						}
						return;
					}

					pack = e.concat(header, payload);
					packet = new DatagramPacket(pack, pack.length,
							new InetSocketAddress(proxyIP, proxyPort));
					sim.sendAfterSim(packet);
					Thread.sleep(delay);
					try{
						socket.setSoTimeout(1000);
					
						socket.receive(packet);
						replies++;
						System.out.println("ping: " +(System.currentTimeMillis()-before) +"ms");
					} catch(SocketTimeoutException ste){
						System.out.println("could not reach destination. packet was either lost or connection was dropped");
					}
					
				} // end for
				float percent = 100 - ((float) replies / (float) pings) * 100;
				System.out.println("packet loss: " + percent + "%");
				z++;

			} else {
				// it is just 1 ping to be sent to the receiver
				// this is what is used to print out the ping as
				// the transfer is happening
				byte[] payload = putLong(System.currentTimeMillis());
				Checksum checksum = new CRC32();
				checksum.update(payload, 0, payload.length);
				long lngChecksum = checksum.getValue();
				Encapsulator e = new Encapsulator(destAddr, "PING", destPort,
						sendingPort, socketAddr, 0, lngChecksum, 0L, "PING");
				header = e.getHeader();
				if (header == null) {
					System.err
							.println("An error occurred creating the header for the packet. ");
					if (socket != null) {
						socket.close();
					}
					return;
				}
				pack = e.concat(header, payload);
				packet = new DatagramPacket(pack, pack.length,
						new InetSocketAddress(proxyIP, proxyPort));
				sim.sendAfterSim(packet);
				Thread.sleep(2);
			}

		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * 
	 * @param command
	 *            - format: SEND path/to/localfile destinationFilename
	 *            destinationAddress destinationPort
	 */
	public void send(String command) {
		String[] s = command.split("\\s");
		// still need to get the packets from the file below
		if (s.length != 5) {

			System.err
					.println("Incorrect number of arguments supplied to send command. ("
							+ s.length
							+ ") supplied.  format: SEND path/to/localfile destinationFilename destinationAddress destinationPort");
			return;
		}
		String locPathname = s[1];
		String destFilename = s[2];
		String destAddr = s[3];
		String destPort = s[4];
		String pingCommand = "ping " + destAddr + " " + destPort + " 1";
		/*---------header + payload = packet----------- */
		byte[] header = new byte[headerSize];
		byte[] payload;
		byte[] pack = new byte[totalPackSize];
		// this is used to make sure that w does not exceed the window size and
		// when it sends the full window size it waits for a response
		int w = 0;
		DatagramPacket received;
		DatagramPacket packet;
		try {
			byte[] socketAddr = InetAddress.getLocalHost().getAddress();

			File f = new File(locPathname);
			if (f.isFile()) {
				InputStream is = new FileInputStream(f);

				long fileSize = f.length();
				// int intFileSize = (int) fileSize;
				// if (intFileSize == fileSize) {
				byte[] fileBytes = new byte[(int) fileSize];
				is.read(fileBytes);
				// loop through file here and put the
				long j = 0;
				long sent = 0L;
				long time;
				boolean pingFlag = false;

				while (j < fileSize) {
					time = System.currentTimeMillis();
					// pings every half a second in
					if (!pingFlag && time % 1000 < 500) {
						ping(pingCommand);
						pingFlag = true;
					} else if (pingFlag && time % 1000 >= 500) {
						pingFlag = false;
					}
					// case where there isnt 1024 bytes left in the file
					if (j + 1024 > fileSize) {
						// only go up to the rest of the file
						long diff = fileSize - j;
						payload = new byte[(int) diff];
						for (int i = 0; i < diff; i++) {
							payload[i] = fileBytes[(int) j];
							j++;
						}
						pack = new byte[(int) (diff + headerSize)];
					} else {
						// normal case
						payload = new byte[1024];
						for (int i = 0; i < 1024; i++) {
							payload[i] = fileBytes[(int) j];
							j++;
						}

					}
					Checksum checksum = new CRC32();
					checksum.update(payload, 0, payload.length);
					//System.out.println(payload.length);
					long lngChecksum = checksum.getValue();
					//System.out.println("sending checksum: " + lngChecksum);
					Encapsulator e = new Encapsulator(destAddr, destFilename,
							destPort, sendingPort, socketAddr, 1, lngChecksum,
							sent + 1, "DATA");
					header = e.getHeader();
					if (header == null) {
						System.err
								.println("An error occurred creating the header for the packet. ");
						if (socket != null) {
							socket.close();
						}
						return;
					}

					// FIRST PACKET
					if (j <= 1024) {
						// send the time to the receiver
						sendTime(destAddr, destFilename, destPort, sendingPort,
								socketAddr);
					}
					// if it is the last change the header to indicate to
					// the receiver that this is the last packet
					if (j == fileSize) {
						// last packet
						e.setlast(2);
						header = e.getHeader();
						//System.out.println("payload.length RS: "+ payload.length);
					}
					pack = e.concat(header, payload);

					packet = new DatagramPacket(pack, pack.length,
							new InetSocketAddress(proxyIP, proxyPort));
					sim.sendAfterSim(packet);
					sent++;
					w++;
					if (w == windowsize) {
						// get ACK before sending next window
						getACK(sent, destAddr, destFilename, destPort,
								socketAddr, j, fileSize, fileBytes);
						w = 0;

					}
					// to ensure throughput doesn't exceed the maximum
					// specified
					Thread.sleep(delay);

				} // end while
				System.out.println("Total Packets sent = " + sent+"                                                    ");
				System.out.println(j + "Bytes");

				/*
				 * } else { System.err
				 * .println("File size too big! Choose a smaller file.");
				 * 
				 * }
				 */
				if (is != null) {
					is.close();
				}

			} else {
				System.err
						.println(locPathname
								+ " is not recognised as a normal file. It may be a directory.");
			}

		} catch (SocketException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("Error sending packet at the socket");

			e.printStackTrace();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	public void sendTime(String destAddr, String destFilename, String destPort,
			int sendingPort, byte[] socketAddr) {
		byte[] receivedPack = new byte[totalPackSize];
		boolean receivedReply = false;
		DatagramPacket received = new DatagramPacket(receivedPack,
				receivedPack.length);
		byte[] payload = putLong(System.currentTimeMillis());
		Checksum checksum = new CRC32();

		checksum.update(payload, 0, payload.length);
		long lngChecksum = checksum.getValue();
		// STME - Start Time packet type
		Encapsulator e = new Encapsulator(destAddr, destFilename, destPort,
				sendingPort, socketAddr, 0, lngChecksum, 0L, "STME");
		byte[] header = e.getHeader();

		byte[] pack = e.concat(header, payload);
		DatagramPacket packet;
		int timeouts=0;
		while (!receivedReply) {
			try {
				packet = new DatagramPacket(pack, pack.length,
						new InetSocketAddress(proxyIP, proxyPort));
				sim.sendAfterSim(packet);
				socket.setSoTimeout(1000);
				socket.receive(received);
				Decapsulator d = new Decapsulator(received.getData());
				// this is where we decide what to do depending on the reply,
				// either: resend the whole window
				// or: do nothing
				String packType = d.getPacketType();
				if (packType.equals("STRP")) {
					// all good start transmission
					//System.out.println("STRP Received in sendTime");
					receivedReply = true;
				} else if(packType.equals("RSND")){
					// reset the timeout
					try {
						socket.setSoTimeout(0);
					} catch (SocketException se) {
						// TODO Auto-generated catch block
						se.printStackTrace();
					}
					continue;
				}
				else {
					System.out
							.println("Send Time: packet type received is not recognised: "
									+ packType);
				}
			} catch (SocketTimeoutException se) {
				packet = new DatagramPacket(pack, pack.length,
						new InetSocketAddress(proxyIP, proxyPort));
				sim.sendAfterSim(packet);
				//TODO the time manipulation thing to make it animate the dots

				System.out.print("Timeout received, Lost connection to the ISS. Reconnecting... " + (++timeouts) + "           "+'\r');
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
			}
			// reset the timeout
			try {
				socket.setSoTimeout(0);
			} catch (SocketException se) {
				// TODO Auto-generated catch block
				se.printStackTrace();
			}

		}//end while
	}

	public void exit(Thread rt, DatagramSocket receiverSocket) {
		// attempt to interrupt the thread for receiving.
		rt.interrupt();
		// this wont be realized unless the thread isn't being
		// blocked by a function call. So we close the socket
		// used to make the receiver so that it can acknowledge
		// the interrupt (as it no longer is being blocked by
		// the call to receive(packet)) and the thread will stop.
		receiverSocket.close();
		if (socket != null) {
			socket.close();
		}
	}

	public byte[] putLong(long value) {
		byte[] array = new byte[1024];
		array[0] = (byte) (0xff & (value >> 56));
		array[1] = (byte) (0xff & (value >> 48));
		array[2] = (byte) (0xff & (value >> 40));
		array[3] = (byte) (0xff & (value >> 32));
		array[4] = (byte) (0xff & (value >> 24));
		array[5] = (byte) (0xff & (value >> 16));
		array[6] = (byte) (0xff & (value >> 8));
		array[7] = (byte) (0xff & value);
		for(int i = 8; i <1024; i++){
			array[i] = (byte)0;
		}
		return array;
	}

	private void getACK(long sent, String destAddr, String destFilename,
			String destPort, byte[] socketAddr, long j, long filesize,
			byte[] fileBytes) {
		boolean receivedACK = false;
		/*---------header + payload = packet----------- */
		byte[] header = new byte[headerSize];
		byte[] payload = new byte[2];
		byte[] pack = new byte[totalPackSize];
		byte[] receivedPack = new byte[totalPackSize];
		DatagramPacket received = new DatagramPacket(receivedPack,
				receivedPack.length);
		Encapsulator e = new Encapsulator(destAddr, destFilename, destPort,
				sendingPort, socketAddr, 1, 0L, sent, "RACK");
		header = e.getHeader();
		pack = e.concat(header, payload);
		DatagramPacket send;
		send = new DatagramPacket(pack, pack.length, new InetSocketAddress(
				proxyIP, proxyPort));
		//System.out.println("Sending RACK for the window:");
		sim.sendAfterSim(send);
		int timeouts=0;
		while (!receivedACK) {
			// receive reply from the receiver about whether
			// everything sent correctly
			// the receiver will send an ACKN along with the number of the
			// packet
			// received in the transmission
			// as a long this needs a timeout of some description in case the
			// packet
			// being sent back is lost
			try {
				socket.setSoTimeout(1000);
				socket.receive(received);
				Decapsulator d = new Decapsulator(received.getData());
				// this is where we decide what to do depending on the reply,
				// either: resend the whole window
				// or: do nothing
				String packType = d.getPacketType();
				if (packType.equals("RSND")) {
					// resend
					//System.out.println("Received Resend request. resending last window.");
					resendWindow(j, filesize, fileBytes, destAddr,
							destFilename, destPort, socketAddr, sent);
				} else if (packType.equals("ACKN")) {
					receivedACK = true;
				} else {
					/*System.out
							.println("Get ACK: Packet Type received is not recognised: "
									+ packType);*/
				}
			} catch (SocketTimeoutException se) {
				System.out.print("Timeout received, Lost connection to the ISS. Reconnecting... " + (++timeouts) + "          "+'\r');
				send = new DatagramPacket(pack, pack.length,
						new InetSocketAddress(proxyIP, proxyPort));
				sim.sendAfterSim(send);
			} catch (IOException ioe) {
				// TODO Auto-generated catch block
				ioe.printStackTrace();
			}
			// reset the timeout
			try {
				socket.setSoTimeout(0);
			} catch (SocketException se) {
				// TODO Auto-generated catch block
				se.printStackTrace();
			}

		}
	}

	private void resendWindow(long j, long fileSize, byte[] fileBytes,
			String destAddr, String destFilename, String destPort,
			byte[] socketAddr, long sent) {
		//System.out.println("Sending last window");
		byte[] header = new byte[headerSize];
		byte[] payload;
		byte[] pack = new byte[totalPackSize];
		DatagramPacket packet;
		long oldj = j;
		long oldsent = sent;
		// reset the j and sent back to what it was before the last window was
		// sent
		j = j - PAYSIZE * windowsize;
		sent = sent - windowsize;
		boolean pingFlag=false;
		String pingCommand = "ping " + destAddr + " " + destPort + " 1";
		long time;
		while (j < oldj) {
			time = System.currentTimeMillis();
			// pings every half a second in
			if (!pingFlag && time % 1000 < 500) {
				ping(pingCommand);
				pingFlag = true;
			} else if (pingFlag && time % 1000 >= 500) {
				pingFlag = false;
			}
			// case where there isnt 1024 bytes left in the file
			if (j + 1024 > fileSize) {
				// only go up to the rest of the file
				long diff = fileSize - j;
				payload = new byte[(int) diff];
				for (int i = 0; i < diff; i++) {
					payload[i] = fileBytes[(int) j];
					j++;
				}
				pack = new byte[(int) (diff + headerSize)];
			} else {
				// normal case
				payload = new byte[1024];
				for (int i = 0; i < 1024; i++) {
					payload[i] = fileBytes[(int) j];
					j++;
				}

			}

			Checksum checksum = new CRC32();
			checksum.update(payload, 0, payload.length);
			long lngChecksum = checksum.getValue();
			Encapsulator e = new Encapsulator(destAddr, destFilename, destPort,
					sendingPort, socketAddr, 1, lngChecksum, sent + 1, "DATA");
			header = e.getHeader();
			if (header == null) {
				System.err
						.println("An error occurred creating the header for the packet. ");
				if (socket != null) {
					socket.close();
				}
				return;
			}

			// FIRST PACKET
			if (j <= 1024) {
				// send the time to the receiver
				sendTime(destAddr, destFilename, destPort, sendingPort,
						socketAddr);
			}
			// if it is the last change the header to indicate to
			// the receiver that this is the last packet
			if (j == fileSize) {
				// last packet
				e.setlast(2);
				header = e.getHeader();
			}
			pack = e.concat(header, payload);
			try {
				packet = new DatagramPacket(pack, pack.length,
						new InetSocketAddress(proxyIP, proxyPort));
				sim.sendAfterSim(packet);
				sent++;
				// to ensure throughput doesn't exceed the maximum
				// specified
				Thread.sleep(delay);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		} // end while
		//System.out.println("window to " + j + " resent");
	}

}
