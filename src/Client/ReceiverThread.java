package Client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import Utilities.Decapsulator;
import Utilities.Encapsulator;

public class ReceiverThread implements Runnable {
	DatagramSocket socket;
	int proxyPort;
	InetAddress proxyIP;
	Map<String, Long> startTimes = new HashMap<String, Long>();
	//Map<Integer, Integer> pingz = new HashMap<Integer, Integer>();
	// map of Ip and portNum of sender to the number of packets received by that
	// client in the
	// latest transmission
	Map<String, Long> packetsReceived = new HashMap<String, Long>();
	int windowsize = 8;
	Map<String, byte[]> currWindow = new HashMap<String, byte[]>();

	// keep a map of the last packet that was sent to a client so if they ask
	// for a resend you can just resend it. --DONT THINK THIS IS USED ANYMORE
	Map<String, DatagramPacket> lastPacketSent = new HashMap<String, DatagramPacket>();
	// map of filenames and a boolean as to whether the current window has a
	// flag to resend the window.
	Map<String, Boolean> windowOk = new HashMap<String, Boolean>();

	public ReceiverThread(DatagramSocket dsocket, int pPort, InetAddress pIP) {
		socket = dsocket;
		proxyPort = pPort;
		proxyIP = pIP;
	}

	public void run() {
		// receives udp packet.
		receive();
	}

	/*
	 * PACKET TYPES AND CODES: PING - PING PingN - NPNG PingReplyN - NPRY
	 * TimeReply - TRPL PingReply - PRPL RequestAcknowledgement - RACK
	 * Acknowledgement ACKN Normal data transfer packet - DATA Start Time - STME
	 * Resend - RSND startup packet send when RS starts up - STRT Start time
	 * reply- STRP
	 */
	public void receive() {
		byte[] buffer = new byte[1024+49];
		byte[] header = new byte[49];
		byte[] payload;
		byte[] pack = new byte[1057];
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
		DatagramPacket send;
		byte[] transmission;
		try {
			int i = 0;
			while (true) {
				socket.receive(packet);
				
				byte[] pdata = packet.getData();
				byte[] data = new byte[packet.getLength()];
				//System.out.println("packet.getlength(): "+packet.getLength());
				System.arraycopy(pdata, 0, data, 0, packet.getLength());
				
				// decapsulate the header
				Decapsulator d = new Decapsulator(data);
				InetAddress srcIP = d.getSourceAddr();
				int srcPort = d.getSourcePort();
				String key = srcIP.toString() + Integer.toString(srcPort);
				String filename = d.getName();
				String pacType = d.getPacketType();
				if (pacType.equals("STRT")) {
					// this is the startup packet, it can be ignored.
					// it is only used for adding the client to the proxys list
					// of available clients
				} else if (pacType.equals("PING")) {
					payload = d.getTransmission();
					//System.out.println("ping command received");
					if (validateChecksum(payload, d.getChecksum())) {
						//System.out.println("ping command checksum validated");
						Encapsulator e = new Encapsulator(d.getSourceAddr()
								.getHostName(), "PINGREPLY", Integer.toString(d
								.getSourcePort() + 1), d.getDestPort(), d
								.getDestAddr().getAddress(), 0, 0L, 0L, "PRPL");
						header = e.getHeader();
						pack = e.concat(header, payload);
						send = new DatagramPacket(pack, pack.length,
								new InetSocketAddress(proxyIP, proxyPort));
						socket.send(send);
					} else {
						// TODO
						// ping data has been corrupted
						//System.out.println("ping command checksum not valid");

					}
				} else if (pacType.equals("NPNG")) {
					// System.out.println("PINGN received");
					payload = d.getTransmission();
					if (validateChecksum(payload, d.getChecksum())) {
						String name = "PINGRPLYN ";
						Encapsulator e = new Encapsulator(d.getSourceAddr()
								.getHostName(), name, Integer.toString(d
								.getSourcePort()), d.getDestPort(), d
								.getDestAddr().getAddress(), 0, 0L, 0L, "NPRY");
						header = e.getHeader();
						pack = e.concat(header, payload);
						send = new DatagramPacket(pack, pack.length,
								new InetSocketAddress(proxyIP, proxyPort));
						socket.send(send);
					} else {
						// TODO:
						// PINGN data has been corrupted
					}
				} else if (pacType.equals("TRPL")) {
					// the transmission carries a Long of the miliseconds
					// taken to send the entire file.
					transmission = d.getTransmission();
					float f = (float) bytesToLong(transmission) / (float) 1000;
					System.out.println("Time taken to send: "
							+ String.format("%.4f", f) + " Seconds");
				} else if (pacType.equals("PRPL")) {
					// the transmission carries the time that the ping command
					// was sent,
					// thus to get the round trip time you minus the current
					// time from
					// the time the initial ping command was sent.
					//System.out.println("ping reply received");
					transmission = d.getTransmission();
					long p = System.currentTimeMillis()
							- bytesToLong(transmission);
					System.out.print("ping: " + p + "ms" + "                                                                "+'\r');
				} else if (pacType.equals("RSND")) {
					DatagramPacket lastSent = lastPacketSent.get(key);
					if (lastSent != null) {
						socket.send(lastSent);
					} else {
						System.out.println("no last packet sent");
					}

				} else if (pacType.equals("RACK")) {
					
					if (packetsReceived.get(key) == d.getID()) {
						// we have received the correct number of packets so far
						// that this RACK is asking for
						if (packetsReceived.get(key) % windowsize == 0) {
							// received the right number of packets for the
							// window
							if (windowOk.get(filename)) {
								// everything in this window went well, add
								// the window to its file and send
								// confirmation to the sender

								// Write the whole transmitted window data
								// to its file
								if (currWindow.get(key).length == 0) {
									// TODO
									// already received this RACK, just send
									// back the ACKN
									sendReply(d, "ACKN");
								} else {
									Files.write(Paths.get(filename),
											currWindow.get(key),
											StandardOpenOption.APPEND,
											StandardOpenOption.CREATE,
											StandardOpenOption.WRITE);

									// reset the window
									currWindow.put(key, new byte[0]);

									// TODO: Send confirmation back to sender to
									// keep going
									sendReply(d, "ACKN");
								}

							} else {
								// reset windowOK to true for next attempt
								resetAll(filename, key);
								sendReply(d, "RSND");
								System.out.println("RESEND: windowOk = false");

							}
						} else {
							// TODO didn't receive enough packets for a full
							// window, must be last window (this may be
							// redundant if we remove the 
							//if (packetsReceived.get(key)% windowsize == 0) check
							
							// Write the whole transmitted window data
							// to its file
							if (currWindow.get(key).length == 0) {
								// TODO
								// already received this RACK, just send
								// back the ACKN
								sendReply(d, "ACKN");
							} else {
								Files.write(Paths.get(filename),
										currWindow.get(key),
										StandardOpenOption.APPEND,
										StandardOpenOption.CREATE,
										StandardOpenOption.WRITE);

								// reset the window
								currWindow.put(key, new byte[0]);

								// TODO: Send confirmation back to sender to
								// keep going
								sendReply(d, "ACKN");
							}
							
						}
					} else {
						// received packets and packets sent dont match up. ask
						// for resend
						resetAll(filename, key);
						sendReply(d, "RSND");
						//System.out.println("RESEND: number of received packets and packets sent dont match up");

					}
				} else if (pacType.equals("STME")) {
					transmission = d.getTransmission();
					if(validateChecksum(transmission,d.getChecksum())){
						long started = bytesToLong(transmission);
						startTimes.put(filename, started);
						windowOk.put(filename, true);
						packetsReceived.put(key, 0L);
						//System.out.println("sending strp");
						sendReply(d, "STRP");
						System.out.println("Receiving file: " + filename);
						
					}
					else{
						sendReply(d,"RSND");
					}
				} else if (pacType.equals("DATA")) {
					// this is a regular packet
					transmission = d.getTransmission();
					if (windowOk.get(filename)||d.getLast()==2) {

						if (validateChecksum(transmission, d.getChecksum())||d.getLast()==2) {
							if (packetsReceived.containsKey(key)||d.getLast()==2) {

								// check that the id of the packet is what we
								// are
								// expecting
								if (d.getID() == (packetsReceived.get(key) + 1L) ||d.getLast()==2) {
									
									packetsReceived.put(key, d.getID());
									// add the packet to the currWindow Received
									currWindow.put(
											key,
											concat(currWindow.get(key),
													transmission));
									i++;
									if (d.getLast() == 2) {
										// last packet
										/*Files.write(Paths.get(filename),
												currWindow.get(key),
												StandardOpenOption.APPEND,
												StandardOpenOption.CREATE,
												StandardOpenOption.WRITE);
												*/
										
										// get total time taken to send file
										long curr = System.currentTimeMillis();
										Long start = startTimes.get(filename);
										if (start != null) {
											long timeTaken = curr - start;

											Encapsulator e = new Encapsulator(
													d.getSourceAddr()
															.getHostName(),
													"TIMEREPLY",
													Integer.toString(d
															.getSourcePort() + 1),
													d.getDestPort(), d
															.getDestAddr()
															.getAddress(), 0,
													0L, 0L, "TRPL");
											header = e.getHeader();

											payload = ByteBuffer.allocate(8)
													.putLong(timeTaken).array();
											pack = e.concat(header, payload);
											send = new DatagramPacket(pack,
													pack.length,
													new InetSocketAddress(
															proxyIP, proxyPort));
											socket.send(send);
											
											System.out.println("file transfer: "+ filename + " complete. "+ packetsReceived.get(key) + " packets received");
											
											// remove all details from the maps of the transfer.
											removeAll(d);
										}
									}

								} else {
									// TODO: send packet to
									// sender to notify them that a packet was
									// received out of order
									windowFailed(d, filename);
									//System.out.println("window failed: packet received out of order");

								}

							} else {
								// this is the first packet received for this
								// file
								// check that the id matches it being the first
								// packet
								if (d.getID() == 1L) {
									currWindow.put(key, transmission);
									
									packetsReceived.put(key, d.getID());

								} else {
									// TODO:
									// notify sender that we didn't receive the
									// first packet, resend the window
									windowFailed(d, filename);
									//System.out.println("window failed: Didn't receive first packet");
								}
							}

						} else {
							// TODO:
							// the data has been corrupted let the sender know
							// to
							// resend the packet
							windowFailed(d, filename);
							if(d.getLast()!=2){
								//System.out.println("window failed: data is corrupt");
							}
						}
					} else if (windowOk.get(filename) == null) {
						// this should never happen as data transmission wont
						// start until the start time is received and replied to
						System.out.println("this should never happen");
					} else { // windowOk set to false for this window
								// do nothing as this window is already set to
								// be resent
					}
				} else {
					System.out.println("Packet Type not recognized");
				}
			}
		} catch (SocketException e) {

			// System.err.println("Error creating new socket for receiving");
			// e.printStackTrace();
		} catch (IOException e) {

			System.err.println("Error receiving packet at the socket");
			e.printStackTrace();
		}
	}

	private void removeAll(Decapsulator d) {
		String filename= d.getName();
		windowOk.remove(filename);
		// this is the key comprised of the clients ip and port number
		InetAddress srcIP = d.getSourceAddr();
		int srcPort = d.getSourcePort();
		String key = srcIP.toString() + Integer.toString(srcPort);

		// remove window
		currWindow.remove(key);
		packetsReceived.remove(key);
		
		
	}

	public long bytesToLong(byte[] bytes) {

		/*
		 * // ByteBuffer buffer = ByteBuffer.allocate(bytes.length); ByteBuffer
		 * buffer = ByteBuffer.wrap(bytes); buffer.put(bytes); buffer.flip();//
		 * need flip return buffer.getLong();
		 */

		return ((long) (bytes[0] & 0xff) << 56)
				| ((long) (bytes[1] & 0xff) << 48)
				| ((long) (bytes[2] & 0xff) << 40)
				| ((long) (bytes[3] & 0xff) << 32)
				| ((long) (bytes[4] & 0xff) << 24)
				| ((long) (bytes[5] & 0xff) << 16)
				| ((long) (bytes[6] & 0xff) << 8) | ((long) (bytes[7] & 0xff));

	}

	/**
	 * 
	 * @param pay
	 *            the payload the checksum is referring to
	 * @param check
	 *            the original checksum to compare with the current one.
	 * @return true if valid otherwise false
	 */
	public boolean validateChecksum(byte[] pay, long check) {
		Checksum checksum = new CRC32();
		
		checksum.update(pay, 0, pay.length);
		//System.out.println("pay.length="+pay.length);
		long lngChecksum = checksum.getValue();
		//System.out.println("packets checksum: "+ check);
		//System.out.println("calculated checksum: "+ lngChecksum);
		if (lngChecksum == check) {
			return true;
		}
		return false;

	}

	public byte[] concat(byte[] A, byte[] B) {
		if(A==null){
			return B;
		}
		int aLen = A.length;
		int bLen = B.length;
		byte[] C = new byte[aLen + bLen];
		System.arraycopy(A, 0, C, 0, aLen);
		System.arraycopy(B, 0, C, aLen, bLen);
		return C;
	}

	public void sendReply(Decapsulator d, String message) throws IOException {
		byte[] header = new byte[39];
		byte[] payload;
		byte[] pack = new byte[1024+39];
		DatagramPacket send;

		// this is the key comprised of the clients ip and port number
		InetAddress srcIP = d.getSourceAddr();
		int srcPort = d.getSourcePort();
		String key = srcIP.toString() + Integer.toString(srcPort);

		// make the packet to send back to notify of a ACK or RSND
		//System.out.println("packets received: "+packetsReceived.get(key));
		Long pr = packetsReceived.get(key);
		if(pr==null){
			pr=0L;
		}
		Encapsulator e = new Encapsulator(d.getSourceAddr().getHostName(),
				message, Integer.toString(d.getSourcePort()), d.getDestPort(),
				d.getDestAddr().getAddress(), 0, 0L, pr, message);
		header = e.getHeader();
		payload = new byte[1024];
		pack = e.concat(header, payload);
		send = new DatagramPacket(pack, pack.length, new InetSocketAddress(
				proxyIP, proxyPort));
		socket.send(send);
		// add the packet to the map of last packet send to that client.
		lastPacketSent.put(key, send);

	}

	private void windowFailed(Decapsulator d, String filename) {
		// the data has been corrupted let the sender know to
		// resend the packet
		// ensure only one reply is sent per window
		windowOk.put(filename, false);
		// this is the key comprised of the clients ip and port number
		InetAddress srcIP = d.getSourceAddr();
		int srcPort = d.getSourcePort();
		String key = srcIP.toString() + Integer.toString(srcPort);
		long received = packetsReceived.get(key);
		// reset the packetsReceived value to remove any of the packets from
		// this window
		packetsReceived.put(key, (received - (received % windowsize)));
		currWindow.put(key, new byte[0]);
	}

	private void resetAll(String filename, String key) {
		windowOk.put(filename, true);
		// reset window
		currWindow.put(key, new byte[0]);
		long received = packetsReceived.get(key);
		// reset the packetsReceived value to remove any of the packets from
		// this window
		packetsReceived.put(key, (received - (received % windowsize)));
	}
}
