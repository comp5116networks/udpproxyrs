package Utilities;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.Timer;
import java.util.TimerTask;

/*********************************************************************************************
 *
 * Functions to simulate signal degradation when connecting to a sender/receiver. Version 3.
 * @author Copyright 2014 Alessandro Jaku
 *
 *********************************************************************************************/

public class SimulatedNetworkConnection {

    private long startTime;		// System time when object was created

    // Constants
    public final int PERIOD = 60;					// Seconds taken for satellite to complete an orbit (actually 5573s)
    public final int DEFAULT_MIN_ISS_DELAY = 2;		// Minimum millisecond delay to ISS (415km=1.4ms)
    public final int DEFAULT_MAX_ISS_DELAY = 5000;	// Maximum millisecond delay to ISS (9000km=30ms + other delays)
    public final int DEFAULT_MIN_NET_DELAY = 0;		// Minimum millisecond delay over network
    public final int DEFAULT_MAX_NET_DELAY = 2000;	// Maximum millisecond delay over network

    public final float ERRORCODE = -1;

    // Properties
//	public boolean spaceStationMode;	// Enable additional simulations, increasing lag and dropouts
//	public int maxAtmInterference = 50;	// Percentage of total strength
    public int offset = 0; 				// Time offset in seconds (e.g. offset of PERIOD/2 starts satellite half-way through its orbit)
    public float riskOfFlares = 0.2f;	// Percentage risk of solar flares corrupting data (guess 1/200 chance)
    public double chanceOfRandomDroppedPackets = 0.15f;	// Percentage risk of packets being lost on network
    public DatagramSocket socket;		// Socket through which to send data


    /**
     * Constructor specifying time offset and maximum atmospheric interference. Space station mode is assumed to be on.
     * @param offset Time offset in seconds (e.g. offset of PERIOD/2 starts satellite half-way through its orbit)
     */
    public SimulatedNetworkConnection ( int offset, DatagramSocket socket ) {
        this.startTime = System.currentTimeMillis();	// Store starting time
        this.socket = socket;
        this.offset = offset;
    }

    /**
     * Send data through the specified socket
     * @param packet Data to send
     */
    public void send( DatagramPacket packet ){
        try {
            socket.send( packet );
        }
        catch ( UnknownHostException e ){
            System.err.println( e.getMessage() );
        }
        catch ( IOException e ) {
            System.err.println( e.getMessage() );
        }
        catch ( Exception e ) {
            System.err.println( e.getMessage() );
        }
    }

    /**
     * Send data after an appropriate delay. Requires socket to be configured beforehand.
     * Delay depends on simulated signal strength.
     * Data may be dropped if simulation requires it.
     * @param packetToSend Data to send
     */
    public void sendAfterSim( final DatagramPacket packetToSend ) {
        DatagramPacket packet = packetToSend;
        double networkInterference = Math.random();

        double networkStrengthPercentage = 100 * (1 - networkInterference);
        int signalStrength = signalStrengthPercentage();
        int corruptionPercentage = getCorruptionPercentage(signalStrength);

        // packet dropped due to no space station line of sight
        if(signalStrength == 0)
        {
            return;
        }
        
        // Simulate random lost packets
        if ( networkStrengthPercentage < chanceOfRandomDroppedPackets ) {
            //System.out.println("Network Packet Dropped");
            return;
        }

        if(networkStrengthPercentage < corruptionPercentage)
        {
            packet = corruptPacket(packetToSend);
        }
        
        send(packet);
    }

    /**
     *
     * @param packet Packet to be corrupted
     * @return A corrupted packet
     */
    private DatagramPacket corruptPacket(DatagramPacket packet)
    {
        byte[] data = packet.getData();
        Random randGen = new Random(System.currentTimeMillis());
        int increment = randGen.nextInt(data.length - 1);
        if(increment<50){
        	increment=55;
        }
        for (int i = increment; i < data.length; i += increment)
        {
            data[i] = (byte)1;
        }
        packet.setData(data);

        return packet;
    }

    private int getCorruptionPercentage(int strength)
    {
        if(strength < 50)
        {
            return 2;
        }
        return 100/strength;
    }

    /**
     * Returns strength as a percentage (0-100) depending on simulated distance.
     * Strength is 0 when satellite is on opposite side of Earth (half the time).
     */
    private int signalStrengthPercentage() {
        long time = TimeUnit.MILLISECONDS.toSeconds( System.currentTimeMillis() - startTime );	// Seconds since object was created
        time += offset;		// Add offset to change starting position
        double strength = 0.95+ Math.cos( 2 * Math.PI * time / PERIOD );
        if ( strength <= 0 | solarFlares() ) {
            return 0;	// No signal
        }
        else {
            return (int) strength;
        }
    }

    /**
     * Returns strength as a fraction (0.0 - 1.0) depending on simulated distance.
     * Strength is 0 when satellite is on opposite side of Earth (half the time).
     * @return
     */
    private float signalStrengthFraction() {
        return signalStrengthPercentage() / 100;
    }

    /**
     * Returns degradation as a percentage (0-100) depending on simulated distance.
     * Degradation = 100% means signal lost.
     * @return
     */
    private int signalDegradationPercentage() {
        return 100 - signalStrengthPercentage();
    }

    /**
     * Returns degradation as a fraction (0.0 - 1.0) depending on simulated distance.
     * Degradation = 1 means signal lost.
     * @return
     */
    private float signalDegradationFraction() {
        return signalDegradationPercentage() / 100;
    }

    /**
     * Returns true if random solar flares should cause packet loss or corruption.
     * @return
     */
    private boolean solarFlares() {
        return Math.random() * 100 < riskOfFlares;
    }
}
	