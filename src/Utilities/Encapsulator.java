package Utilities;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by josh on 5/09/14.
 */

/*
    A simple class to automatically encapsulate the header data into a byte array
 */
public class Encapsulator
{
	private int HEADER_SIZE = 49;
	private String destAddr;
	private String destPort;
	private byte [] sourceAddr;
	private int sourcePort;
	private String filename;	
	private Integer last;
	private long checksum;
	private long id;
	private String packetType;
	
    public Encapsulator(String destAddr , String destFilename,
                        String destPort, int sendingPort, byte[] socketAddr, int last, long checksum, long id, String packetType)
    {
    	this.filename=destFilename;
    	this.destAddr = destAddr;
    	this.destPort = destPort;
    	this.sourcePort = sendingPort;
    	this.sourceAddr=socketAddr;
    	this.last = last;
    	this.checksum = checksum;
    	this.id = id;
    	this.packetType = packetType;
    }
    
    public byte[] getHeader(){
    	return makeHeader(filename, destAddr, destPort, sourcePort, sourceAddr, checksum, id, packetType);
    }
    
	/**
	 *
	 * @param destFilename
	 *            name for the file on the receiver side
	 * @param destAddr
	 *            destination IP address
	 * @param destPort
	 *            destination Port
	 * @param socketAddr
	 *            IP address of this sender
	 * @return
	 */
	public byte[] makeHeader(String destFilename,
			String destAddr, String destPort, int sendingPort, byte[] socketAddr, long checksum, long id, String pacType) {
		InetAddress destIpAddr;

		if (destFilename.length() <= 20) {
			/*---------------header elements----------------*/
			byte[] destAddrBytes = new byte[4];
			byte[] destPortBytes = new byte[2];
			byte[] sourcePortBytes = new byte[2];
			byte[] nameBytes = new byte[20];
			byte lastByte;
			byte[]checkBytes = new byte[4];
			byte[] idBytes = new byte[8];
			byte[] pacTypeBytes= new byte[4];
			/*---------header + payload = packet----------- */
			byte[] header = new byte[HEADER_SIZE];
			try {
				destIpAddr = InetAddress.getByName(destAddr);
				/*----------elements of the header--------*/
				destAddrBytes = destIpAddr.getAddress();
				destPortBytes = intToByteArray(Integer.parseInt(destPort));
				sourcePortBytes = intToByteArray(sendingPort);
				System.arraycopy(destFilename.getBytes(), 0, nameBytes, 0,
						destFilename.length());
				lastByte=last.byteValue();
				checkBytes = putCheckLong(checksum);
				idBytes = putLong(id);
				System.arraycopy(pacType.getBytes(), 0, pacTypeBytes, 0, 4);
				// put the header together (various elements of the header)
				header = concatNine(destAddrBytes, destPortBytes, socketAddr,
						sourcePortBytes, nameBytes, lastByte, checkBytes, idBytes, pacTypeBytes);
				//System.out.println("encapsulator header.len:"+header.length);
				return header;
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			System.err
					.println("Destination file name must be less than 20 charcters long.");
		}
		return null; // error occured somewhere

	}
	public void setlast(int setTo){
		last=setTo;
	}
	byte[] concatNine(byte[] A, byte[] B, byte[] C, byte[] D, byte[] E, byte last, byte[] check, byte[] id, byte[] pacType) {
		int aLen = A.length;
		int bLen = B.length;
		int cLen = C.length;
		int dLen = D.length;
		int eLen = E.length;
		int checkLen = check.length;
		int idLen = id.length;
		int pacTypeLen = pacType.length;
		
		
		int len =aLen + bLen + cLen + dLen + eLen+1 + checkLen+ idLen+pacTypeLen;
		
		byte[] F = new byte[len];
		System.arraycopy(A, 0, F, 0, aLen);
		System.arraycopy(B, 0, F, aLen, bLen);
		System.arraycopy(C, 0, F, aLen + bLen, cLen);
		System.arraycopy(D, 0, F, aLen + bLen + cLen, dLen);
		System.arraycopy(E, 0, F, aLen + bLen + cLen + dLen, eLen);
		F[aLen + bLen + cLen + dLen + eLen] = last;
		System.arraycopy(check, 0, F, aLen + bLen + cLen + dLen +eLen+1, checkLen);
		System.arraycopy(id, 0, F, aLen + bLen + cLen + dLen +eLen+1+checkLen, idLen);
		System.arraycopy(pacType, 0, F, aLen + bLen + cLen + dLen +eLen+1+checkLen+idLen, pacTypeLen);
		//System.out.println("encapsulator f.len: "+F.length);
		return F;
		
	}
	public static byte[] intToByteArray(int a) {
		byte[] ret = new byte[2];
		ret[1] = (byte) (a & 0xFF);
		ret[0] = (byte) ((a >> 8) & 0xFF);
		return ret;
	}

	public byte[] concat(byte[] A, byte[] B) {
		int aLen = A.length;
		int bLen = B.length;
		if(B.length!=1024){
			//System.out.println("B.length "+B.length);
		}
		byte[] C = new byte[aLen + bLen];
		System.arraycopy(A, 0, C, 0, aLen);
		System.arraycopy(B, 0, C, aLen, bLen);
		return C;
	}

	public byte[] putCheckLong(long value) {
		byte[] array = new byte[4];
		array[0] = (byte) (0xff & (value >> 24));
		array[1] = (byte) (0xff & (value >> 16));
		array[2] = (byte) (0xff & (value >> 8));
		array[3] = (byte) (0xff & value);
		//to determine which 4 bytes to throw away
		//System.out.println("array[0] = " + array[0]+"array[1] = " + array[1]+"array[2] = " + array[2]+"array[3] = " + array[3]);
		return array;
	}
	public byte[] putLong(long value) {
		byte[] array = new byte[8];
		array[0] = (byte) (0xff & (value >> 56));
		array[1] = (byte) (0xff & (value >> 48));
		array[2] = (byte) (0xff & (value >> 40));
		array[3] = (byte) (0xff & (value >> 32));
		array[4] = (byte) (0xff & (value >> 24));
		array[5] = (byte) (0xff & (value >> 16));
		array[6] = (byte) (0xff & (value >> 8));
		array[7] = (byte) (0xff & value);
		return array;
	}

}
