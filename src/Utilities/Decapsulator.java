package Utilities;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static java.net.InetAddress.getByAddress;

public class Decapsulator {
	private byte[] destAddrBytes = new byte[4];
	private byte[] destPortBytes = new byte[2];
	private byte[] sourceAddr = new byte[4];
	private byte[] sourcePortBytes = new byte[2];
	private byte[] nameBytes = new byte[20];
	private byte[] checkBytes = new byte[4];
	private byte[] idBytes= new byte[8];
	private byte[] pacTypeBytes = new byte[4];
	private byte[] transmission;
	private Byte lastByte;
	private int HEADER_SIZE = 49;
	public Decapsulator(byte[] data){
		int len = data.length;
		//System.out.println("DECAP: len: "+len+", len-HEADER_SIZE: "+(len-HEADER_SIZE));
		
		transmission = new byte[len - HEADER_SIZE];
		// Decompose the message into the header components and the
		// transmission data
		//--------------------------------------------------//
		System.arraycopy(data, 0, destAddrBytes, 0, 4);
		System.arraycopy(data, 4, destPortBytes, 0, 2);
		System.arraycopy(data, 6, sourceAddr, 0, 4);
		System.arraycopy(data, 10, sourcePortBytes, 0, 2);
		System.arraycopy(data, 12, nameBytes, 0, 20);
		lastByte=data[32];
		System.arraycopy(data, 33, checkBytes, 0, 4);
		System.arraycopy(data, 37, idBytes, 0, 8);
		System.arraycopy(data, 45, pacTypeBytes, 0, 4);
		System.arraycopy(data, HEADER_SIZE, transmission, 0, len - HEADER_SIZE);
      //-------------------------------------------------//
  }
    public InetAddress getDestAddr() {
        return getInet(destAddrBytes);
    }
    public int getDestPort() {
        return bytesToInt(destPortBytes);
    }
    public InetAddress getSourceAddr() {
        return getInet(sourceAddr);
    }
    public int getSourcePort() {
        return bytesToInt(sourcePortBytes);
    }
    public String getName()
    {
        return (new String(nameBytes).trim());
    }
    public byte[] getTransmission() {
        return transmission;
    }
    public int getLast(){
    	return lastByte.intValue();
    }
    public long getID(){
    	return bytesToLong(idBytes);
    }
    
    public long getChecksum(){
    	return ((long) (checkBytes[0] & 0xff) << 24)
				| ((long) (checkBytes[1] & 0xff) << 16)
				| ((long) (checkBytes[2] & 0xff) << 8) | ((long) (checkBytes[3] & 0xff));
    	
    }
    public String getPacketType(){
    	return (new String(pacTypeBytes));
    }
    
    protected InetAddress getInet(byte [] bytes)
    {
        try
        {
            return getByAddress(sourceAddr);
        } catch (UnknownHostException e)
        {
            System.err.println("Packet source address unknown");
            e.printStackTrace();
        }
        return null;
    }

    protected int bytesToInt(byte [] bytes)
    {
        if (bytes.length != 2)
        {
            System.err.println("Byte array invalid length when converting to int");
            return 0;
        }
        int value = 0;
        for (int i = 0; i < 2; i++) {
            int shift = (2 - 1 - i) * 8;
            value += (bytes[i] & 0x000000FF) << shift;
        }
        return value;
    }
    
	public long bytesToLong(byte[] bytes) {

		/*
		 * // ByteBuffer buffer = ByteBuffer.allocate(bytes.length); ByteBuffer
		 * buffer = ByteBuffer.wrap(bytes); buffer.put(bytes); buffer.flip();//
		 * need flip return buffer.getLong();
		 */

		return ((long) (bytes[0] & 0xff) << 56)
				| ((long) (bytes[1] & 0xff) << 48)
				| ((long) (bytes[2] & 0xff) << 40)
				| ((long) (bytes[3] & 0xff) << 32)
				| ((long) (bytes[4] & 0xff) << 24)
				| ((long) (bytes[5] & 0xff) << 16)
				| ((long) (bytes[6] & 0xff) << 8) | ((long) (bytes[7] & 0xff));

	}

}
