# COMP5116 Design of Networks & Distributed Systems #
# Semester 2, 2014 #
## **Programming Assignment 2** ##
In previous assignment, we used TCP, a stream‐based protocol that ensures reliable data transfer.
On the other hand, UDP is a simple datagram‐based protocol but does not guarantee any reliability.
UDP packets are almost never lost in LAN but can be lost in a congested network environment or in a
WAN. This assignment focuses on simulating an unreliable network condition.

**Part 1**

Create a proxy server and the receiver/sender (RS) that runs on UDP.
The RS is to send files to one another (bidirectional communication). However, all RSs
communications must go through the proxy server. The proxy server is to relay data between the
RSs and must accept connection from unlimited numbers of RSs. This proxy server will then induce
an unreliable network condition by randomly dropping packets AND delaying packets.
When executing the RS, it must accept a parameter defining the maximum line throughput.
Example: 400 kilobit/s. This throughput must be obeyed by the RS during data transmission.
You will also need to devise a mechanism to be able to discover the connected RSs and then to
send/receive files from them.

**Part 2**

Create a diagnostic function in your RS. The sender will use this function to “ping” the receiver while
sending data to determine its round trip time (RTT). For more accurate measurement, only the
successfully received reply is measured. At a suitable interval, output the average RTT time to your
console or GUI. The output should refresh at the same line rather than printing a new line. Unit of
measurement should be in kilobit/s. At the end of each file transfer, output the time taken to
transfer the file.

**Hints/Notes:**

* The RS must ping the RTT on its own. The proxy cannot send the generated random delay figures (or any other simulated numbers) to the RS. This is considered as “cheating”.

* The sample code ProgressBar.java shows you how to overwrite same line in a console output. Be warned this does not work with Eclipse.

**Part 3 **

A Delay Tolerant Network (DTN) is a computer network that operates in extreme condition. It does
not have constant connectivity and its performance and delay can vary significantly. Consider this:
The International Space Station (ISS) is placed in the low earth orbit and wishes to receive files from
Earth’s ground stations. However, the ISS orbits very fast and changes its position relative a ground
station very quickly. So to maximize connectivity, a number of satellite ground stations located at
different locations are used to communicate with the ISS.

a.) You are to modify your proxy and receiver/sender (RS) to simulate these conditions:  

* Any ground station can transmit files to the ISS and there are multiple ground stations.
* The ground station link to the ISS can have varying performance and delay (eg. slow link/fast link/delay in minutes or hours). These can be due to different weather conditions such as clouds or rains hindering direct line‐of‐sight to the ISS.
* Connectivity to the ISS is lost once the ISS orbits away from the ground stations, even in themidst of data transfer.
* Solar flares corrupt a part or the entire data transmission.
* Even though our case requires file transfer from ground stations to the ISS, the ISS can still transmit data to the ground stations. These data can include line status or data specific to connection management.

b.) Then, extend your diagnostic function to report any additional important link condition. For
example, link availability to check if the ISS has moved away.

c.) Finally, write in about half a page or one page report on how you simulate these conditions.  
Describe the model you used and any other assumptions you made. Also, list any addition to your
diagnostic function.

**Hints/Notes: **

* Each RS instance can be considered as a ground station.

* You may use the same RS implementation for both ISS and ground stations, or you can have different implementations for each.

* The RSs must not know any simulation details. All simulations are done inside the proxy.

* For the purpose of demonstration, we can shorten timing to reasonable amount (eg. 10 seconds to orbit away from a station) instead of hours or days. You’re not expected to have accurate “physics” modelling in this sense.

# COMP5116 Design of Networks & Distributed Systems #
# Semester 2, 2014  

## **Programming Assignment 3** ##

In previous assignment, we used UDP protocol and simulated unreliable network conditions. We
further extended the simulation to the International Space Station (ISS). However, our received file was
corrupted. For this assignment, you are to design your own DTN architecture and protocol for use with
the ISS. (10%)

The aim of your architecture and protocol is to:

*  Ensure reliable data delivery from the ground stations to the ISS. The file received must be
* complete and correct.
*  Maximize data transfer performance. These include maximum connectivity to the ISS and
* complete file transfer in the shortest possible time.
*  Ensure scalability with possible addition of more ground stations at any time.

The protocol needs to work in the following scenarios:

* The file size can be very large to the magnitude of more than yottabye!
* Any ground station can initialize file transfer to the ISS.
*  Multiple ground stations may come into contact with ISS if there is coverage.
*  Ground stations can work together to maximize connectivity and transfer performance.
* There may be hundreds of ground stations so your solution must be scalable. For example, replicating an entire file across all the ground stations is not scalable.

Modify the receiver/sender (RS) and proxy you had in previous assignment to implement this new architecture and protocol you designed. You can choose the best simulation model from your group mate to work with. Give your protocol a meaningful name. 

Finally, write a few pages of report describing your architecture and protocol. This is an open ended question so you will be evaluated based on the ingenuity, reliability, scalability and performance of your protocol.

**Notes/Hints:**

*  All conditions on the ISS in previous assignment still apply here.
*  Unless you are changing your simulated environment, the proxy should have minimal or virtually no modification at this point. Your proxy’s functionality is ONLY to simulate packet loss/delay.
* For the purpose of this simulation, ground stations can discover each other via the proxy or on a LAN 255.255.255.255 broadcast. Communication between ground stations can be TCP or UDP.
*  Your report should mention the overall architecture, how your protocol works, why you design it that way, any assumptions or models used, and its limitations.
* The diagnostic functions are useful for RS decision making.

**Submission**

This part of the programming assignment must be demonstrated to the tutor and a copy of the code
must be submitted no later than Week 12: Thursday 23 October. Demonstrate your protocol by
transferring a 100MB file with at least 8 ground stations and checking if you have the same MD5 file
hash signature from the original file. The ground stations should be working together.